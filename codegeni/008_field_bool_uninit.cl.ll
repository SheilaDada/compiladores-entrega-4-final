@ImpossibleDreamstr = constant [17 x i8] c"Impossible Dream\00"
@ImpossibleDream = global i8* bitcast([17 x i8]* @ImpossibleDreamstr  to i8*)
@Anotheronebitestheduststr = constant [27 x i8] c"Another one bites the dust\00"
@Anotheronebitesthedust = global i8* bitcast([27 x i8]* @Anotheronebitestheduststr  to i8*)
%Object = type { i8* }
%IO = type { i8* }
declare %Object* @Object_abort(%Object*)
declare i8* @Object_type_name(%Object*)
declare %IO* @IO_out_string(%IO*, i8*)
declare %IO* @IO_out_int(%IO*, i32 )
declare i8* @IO_in_string(%IO*)
declare i32 @IO_in_int(%IO*)
declare i32 @String_length(i8*)
declare i8* @String_concat(i8*, i8*)
declare i8* @String_substr(i8*, i32, i32 )
%Main = type { i8* }
define i32 @main() {
%_tmp_1 = bitcast %Main* null to %IO*
call %Main* @Main_main(%Main* null)
ret i32 0
}
@flag = global i1 0
define %Main* @Main_main(%Main* %m) {
%_tmp_1 = bitcast %Main* %m to %IO*
%lflag0 = load i1* @flag
br i1 %lflag0, label %Verdadeif0, label %Falsoif0
Verdadeif0:
%lImpossibleDream = load i8** @ImpossibleDream
call %IO* @IO_out_string(%IO* null, i8* %lImpossibleDream)
br label %Jumpif0
Falsoif0:
%lAnotheronebitesthedust = load i8** @Anotheronebitesthedust
call %IO* @IO_out_string(%IO* null, i8* %lAnotheronebitesthedust)
br label %Jumpif0
Jumpif0:
ret %Main* %m
}
