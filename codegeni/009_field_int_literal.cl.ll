@Tamaniodoisptostr = constant [10 x i8] c"Tamanio: \00"
@Tamaniodoispto = global i8* bitcast([10 x i8]* @Tamaniodoisptostr  to i8*)
%Object = type { i8* }
%IO = type { i8* }
declare %Object* @Object_abort(%Object*)
declare i8* @Object_type_name(%Object*)
declare %IO* @IO_out_string(%IO*, i8*)
declare %IO* @IO_out_int(%IO*, i32 )
declare i8* @IO_in_string(%IO*)
declare i32 @IO_in_int(%IO*)
declare i32 @String_length(i8*)
declare i8* @String_concat(i8*, i8*)
declare i8* @String_substr(i8*, i32, i32 )
%Main = type { i8* }
define i32 @main() {
%_tmp_1 = bitcast %Main* null to %IO*
store i32 38, i32* @size
call %Main* @Main_main(%Main* null)
ret i32 0
}
@size = global i32 0
define %Main* @Main_main(%Main* %m) {
%_tmp_1 = bitcast %Main* %m to %IO*
%lTamaniodoispto = load i8** @Tamaniodoispto
call %IO* @IO_out_string(%IO* null, i8* %lTamaniodoispto)
%lsize1 = load i32* @size
call %IO* @IO_out_int(%IO* null, i32 %lsize1)
ret %Main* %m
}
