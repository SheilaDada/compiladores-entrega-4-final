@Thetruthisstr = constant [14 x i8] c"The truth is \00"
@Thetruthis = global i8* bitcast([14 x i8]* @Thetruthisstr  to i8*)
@Truestr = constant [5 x i8] c"True\00"
@True = global i8* bitcast([5 x i8]* @Truestr  to i8*)
@Falsestr = constant [6 x i8] c"False\00"
@False = global i8* bitcast([6 x i8]* @Falsestr  to i8*)
%Object = type { i8* }
%IO = type { i8* }
declare %Object* @Object_abort(%Object*)
declare i8* @Object_type_name(%Object*)
declare %IO* @IO_out_string(%IO*, i8*)
declare %IO* @IO_out_int(%IO*, i32 )
declare i8* @IO_in_string(%IO*)
declare i32 @IO_in_int(%IO*)
declare i32 @String_length(i8*)
declare i8* @String_concat(i8*, i8*)
declare i8* @String_substr(i8*, i32, i32 )
%Main = type { i8* }
define i32 @main() {
%_tmp_1 = bitcast %Main* null to %IO*
store i1 1, i1* @truth
call %Main* @Main_main(%Main* null)
ret i32 0
}
@truth = global i1 0
define %Main* @Main_main(%Main* %m) {
%_tmp_1 = bitcast %Main* %m to %IO*
%lThetruthis = load i8** @Thetruthis
call %IO* @IO_out_string(%IO* null, i8* %lThetruthis)
%ltruth1 = load i1* @truth
br i1 %ltruth1, label %Verdadeif1, label %Falsoif1
Verdadeif1:
%lTrue = load i8** @True
call %IO* @IO_out_string(%IO* null, i8* %lTrue)
br label %Jumpif1
Falsoif1:
%lFalse = load i8** @False
call %IO* @IO_out_string(%IO* null, i8* %lFalse)
br label %Jumpif1
Jumpif1:
%resultIf1 = phi %Object* [null, %Verdadeif1 ], [null, %Falsoif1]
ret %Main* %m
}
