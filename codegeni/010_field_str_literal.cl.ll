@greenstr = constant [6 x i8] c"green\00"
@green = global i8* bitcast([6 x i8]* @greenstr  to i8*)
@Thisappleisstr = constant [15 x i8] c"This apple is \00"
@Thisappleis = global i8* bitcast([15 x i8]* @Thisappleisstr  to i8*)
%Object = type { i8* }
%IO = type { i8* }
declare %Object* @Object_abort(%Object*)
declare i8* @Object_type_name(%Object*)
declare %IO* @IO_out_string(%IO*, i8*)
declare %IO* @IO_out_int(%IO*, i32 )
declare i8* @IO_in_string(%IO*)
declare i32 @IO_in_int(%IO*)
declare i32 @String_length(i8*)
declare i8* @String_concat(i8*, i8*)
declare i8* @String_substr(i8*, i32, i32 )
%Main = type { i8* }
define i32 @main() {
%_tmp_1 = bitcast %Main* null to %IO*
call %Main* @Main_main(%Main* null)
ret i32 0
}
@colorstr = constant [6 x i8] c"green\00"
@color = global i8* bitcast([6 x i8]* @colorstr  to i8*)
define %Main* @Main_main(%Main* %m) {
%_tmp_1 = bitcast %Main* %m to %IO*
%lThisappleis = load i8** @Thisappleis
call %IO* @IO_out_string(%IO* null, i8* %lThisappleis)
%lcolor1 = load i8** @color
call %IO* @IO_out_string(%IO* null, i8* %lcolor1)
ret %Main* %m
}
