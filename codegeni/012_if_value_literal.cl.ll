@Iaspinhamastr = constant [7 x i8] c"I'm a \00"
@Iaspinhama = global i8* bitcast([7 x i8]* @Iaspinhamastr  to i8*)
@Marvelstr = constant [7 x i8] c"Marvel\00"
@Marvel = global i8* bitcast([7 x i8]* @Marvelstr  to i8*)
@DCstr = constant [3 x i8] c"DC\00"
@DC = global i8* bitcast([3 x i8]* @DCstr  to i8*)
@Sinceyearstr = constant [13 x i8] c"\0ASince year \00"
@Sinceyear = global i8* bitcast([13 x i8]* @Sinceyearstr  to i8*)
%Object = type { i8* }
%IO = type { i8* }
declare %Object* @Object_abort(%Object*)
declare i8* @Object_type_name(%Object*)
declare %IO* @IO_out_string(%IO*, i8*)
declare %IO* @IO_out_int(%IO*, i32 )
declare i8* @IO_in_string(%IO*)
declare i32 @IO_in_int(%IO*)
declare i32 @String_length(i8*)
declare i8* @String_concat(i8*, i8*)
declare i8* @String_substr(i8*, i32, i32 )
%Main = type { i8* }
define i32 @main() {
%_tmp_1 = bitcast %Main* null to %IO*
call %Main* @Main_main(%Main* null)
ret i32 0
}
define %Main* @Main_main(%Main* %m) {
%_tmp_1 = bitcast %Main* %m to %IO*
%lIaspinhama = load i8** @Iaspinhama
call %IO* @IO_out_string(%IO* null, i8* %lIaspinhama)
br i1 1, label %Verdadeif1, label %Falsoif1
Verdadeif1:
%lMarvel = load i8** @Marvel
br label %Jumpif1
Falsoif1:
%lDC = load i8** @DC
br label %Jumpif1
Jumpif1:
%resultIf1 = phi i8* [%lMarvel, %Verdadeif1 ], [%lDC, %Falsoif1]
call %IO* @IO_out_string(%IO* null, i8* %resultIf1)
%lSinceyear = load i8** @Sinceyear
call %IO* @IO_out_string(%IO* null, i8* %lSinceyear)
br i1 0, label %Verdadeif3, label %Falsoif3
Verdadeif3:
br label %Jumpif3
Falsoif3:
br label %Jumpif3
Jumpif3:
%resultIf3 = phi i32 [1978, %Verdadeif3 ], [3915, %Falsoif3]
call %IO* @IO_out_int(%IO* null, i32 %resultIf3)
ret %Main* %m
}
