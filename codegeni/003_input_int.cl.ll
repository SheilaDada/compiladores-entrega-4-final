%Object = type { i8* }
%IO = type { i8* }
declare %Object* @Object_abort(%Object*)
declare i8* @Object_type_name(%Object*)
declare %IO* @IO_out_string(%IO*, i8*)
declare %IO* @IO_out_int(%IO*, i32 )
declare i8* @IO_in_string(%IO*)
declare i32 @IO_in_int(%IO*)
declare i32 @String_length(i8*)
declare i8* @String_concat(i8*, i8*)
declare i8* @String_substr(i8*, i32, i32 )
%Main = type { i8* }
define i32 @main() {
%_tmp_1 = bitcast %Main* null to %IO*
call %Main* @Main_main(%Main* null)
ret i32 0
}
define %Main* @Main_main(%Main* %m) {
%_tmp_1 = bitcast %Main* %m to %IO*
%inInt0 = call i32 @IO_in_int(%IO* null)
call %IO* @IO_out_int(%IO* null, i32 %inInt0)
ret %Main* %m
}
