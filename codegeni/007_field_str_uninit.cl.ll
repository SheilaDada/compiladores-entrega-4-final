@colchdirstr = constant [2 x i8] c"[\00"
@colchdir = global i8* bitcast([2 x i8]* @colchdirstr  to i8*)
@colcheisstr = constant [2 x i8] c"]\00"
@colcheis = global i8* bitcast([2 x i8]* @colcheisstr  to i8*)
%Object = type { i8* }
%IO = type { i8* }
declare %Object* @Object_abort(%Object*)
declare i8* @Object_type_name(%Object*)
declare %IO* @IO_out_string(%IO*, i8*)
declare %IO* @IO_out_int(%IO*, i32 )
declare i8* @IO_in_string(%IO*)
declare i32 @IO_in_int(%IO*)
declare i32 @String_length(i8*)
declare i8* @String_concat(i8*, i8*)
declare i8* @String_substr(i8*, i32, i32 )
%Main = type { i8* }
define i32 @main() {
%_tmp_1 = bitcast %Main* null to %IO*
call %Main* @Main_main(%Main* null)
ret i32 0
}
@messagestr = constant [1 x i8] c"\00"
@message = global i8* bitcast([1 x i8]* @messagestr to i8*)
define %Main* @Main_main(%Main* %m) {
%_tmp_1 = bitcast %Main* %m to %IO*
%lcolchdir = load i8** @colchdir
call %IO* @IO_out_string(%IO* null, i8* %lcolchdir)
%lmessage1 = load i8** @message
call %IO* @IO_out_string(%IO* null, i8* %lmessage1)
%lcolcheis = load i8** @colcheis
call %IO* @IO_out_string(%IO* null, i8* %lcolcheis)
ret %Main* %m
}
