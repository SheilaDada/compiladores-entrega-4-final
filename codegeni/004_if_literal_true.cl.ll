@truestr = constant [5 x i8] c"true\00"
@true = global i8* bitcast([5 x i8]* @truestr  to i8*)
@falsestr = constant [6 x i8] c"false\00"
@false = global i8* bitcast([6 x i8]* @falsestr  to i8*)
%Object = type { i8* }
%IO = type { i8* }
declare %Object* @Object_abort(%Object*)
declare i8* @Object_type_name(%Object*)
declare %IO* @IO_out_string(%IO*, i8*)
declare %IO* @IO_out_int(%IO*, i32 )
declare i8* @IO_in_string(%IO*)
declare i32 @IO_in_int(%IO*)
declare i32 @String_length(i8*)
declare i8* @String_concat(i8*, i8*)
declare i8* @String_substr(i8*, i32, i32 )
%Main = type { i8* }
define i32 @main() {
%_tmp_1 = bitcast %Main* null to %IO*
call %Main* @Main_main(%Main* null)
ret i32 0
}
define %Main* @Main_main(%Main* %m) {
%_tmp_1 = bitcast %Main* %m to %IO*
br i1 1, label %Verdadeif0, label %Falsoif0
Verdadeif0:
%ltrue = load i8** @true
call %IO* @IO_out_string(%IO* null, i8* %ltrue)
br label %Jumpif0
Falsoif0:
%lfalse = load i8** @false
call %IO* @IO_out_string(%IO* null, i8* %lfalse)
br label %Jumpif0
Jumpif0:
ret %Main* %m
}
