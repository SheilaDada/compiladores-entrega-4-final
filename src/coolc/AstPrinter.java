package coolc;

import coolc.ast.*;
import java.util.ArrayList;
import java.util.Arrays;


public class AstPrinter {

    private Program _root;
    private boolean _printTypes;

    public AstPrinter(Program root) {
        this(root, false);
    }

    public AstPrinter(Program root, boolean printTypes) {
        _root = root;
        _printTypes = printTypes;
    }

    public void print() {
        //System.out.println("program");
        System.out.println("%Object = type { i8* }");
        System.out.println("%IO = type { i8* }");
        System.out.println("declare %Object* @Object_abort(%Object*)");
        System.out.println("declare i8* @Object_type_name(%Object*)");
        System.out.println("declare %IO* @IO_out_string(%IO*, i8*)"); 
        System.out.println("declare %IO* @IO_out_int(%IO*, i32 )"); 
        System.out.println("declare i8* @IO_in_string(%IO*)"); 
        System.out.println("declare i32 @IO_in_int(%IO*)"); 
        System.out.println("declare i32 @String_length(i8*)"); 
        System.out.println("declare i8* @String_concat(i8*, i8*)"); 
        System.out.println("declare i8* @String_substr(i8*, i32, i32 )");
        
        for(ClassDef c: _root) {            
            print(c);
        }
    }
    


    /*********************** CLASS ************************************/
    // Considerando que sólo hay una clase y una función main
    private void print(ClassDef c) {

        System.out.println("%Main = type { i8* }");

        System.out.println("define i32 @main() {");
        
        System.out.println("%_tmp_1 = bitcast %Main* null to %IO*");

        for(Feature f: c.getBody()) {
            // 0 significa que no va a ser hecho un nuevo bloco
            print2(f, c.getType());
        }
        
        // É a classe main e só retorna 0 e já é do tipo int
        System.out.println("ret i32 0");
        System.out.println("}");
        
        for(Feature d: c.getBody()) {
            // 0 significa que no va a ser hecho un nuevo bloco
            print(d, c.getType());
        }

    }

    /*********************** FEATURE - PRINT ************************************/
    private void print(Feature f, String classtype ) {
        Expr e = null;
        String resultado = null;
        if(f instanceof Method) {
            Method m = (Method)f;
             if (m.getBody() instanceof Expr)
                 e = (Expr)m.getBody();

            //mismo tipo de retorno que la función
             if(!m.getName().equals("main")){
                System.out.printf("define" + tipo(m.getType()) + " @" + m.getName() + "(" );

                for(int n = 0; n < m.getParams().size(); n ++) {
                    //System.out.printf("%s %s -> ", var.getType(), var.getId());
                    //problema com o print da vígula
                    m._params.get(n);
                    System.out.printf(tipo(m._params.get(n).getType()) + " %" + m._params.get(n).getId());
                    if (n != (m.getParams().size() - 1))
                        System.out.printf(", ");
                    if( m._params.get(n).getValue() != null ){
                        throw new RuntimeException("WTF? initializing parameter definition?");
                    }
                }
                //System.out.println(m.getType());
                System.out.println(") {");


                //FALTA VER COMO DEVOLVE AQUI SHEILA
                resultado = print(m.getBody(), 1, 0);
                // devuelve el puntero nulo
                /*if (m.getType().equals(classtype)){
                    System.out.println ("ret %" + classtype + " null");
                }*/
                // AINDA PRECISA IMPLEMENTAR, BODY É UM TIPO DE EXPRESSAO SHEILA
                //else{
                if(!m.getType().equals("Object"))
                         System.out.println("ret %" + m.getType() + " " + resultado );
                else
                    System.out.println("ret %" + m.getType() + " null");
                //}

                System.out.println("}");
             }
             
             else{
                 System.out.println("define %Main* @Main_main(%Main* %m) {");
                 System.out.println("%_tmp_1 = bitcast %Main* %m to %IO*");
                 if(m.getBody() != null)
                    resultado = print(m.getBody(), 1, 0);
                 System.out.println("ret %Main* %m");
                 System.out.println("}");
             }
        }
        else if (f instanceof Variable) {
            Variable var = (Variable)f;
            //printIndent(2);
            //System.out.printf("field %s %s\n", var.getType(), var.getId());
            
            // AQUI PRECISA IMPLEMENTAR, NÃO SEI COMO VAI VOLTAR O RESULTADO SHEILA
                // conta que com string não tem conta, vai ficar muito complicado se tiver
             if(var.getType().equals("String")){

                 if(var.getValue() != null){                     
                     if (var.getValue() instanceof ValueExpr){
                         ValueExpr value = (ValueExpr)var.getValue();
                         
                         if(value.getExprType().equals("String")){
                             String hola = (String)value.getValue();
                             hola =  hola.replace("\n", "\\0A").replace("\f", "\\0C").replace("\t", "\\09").replace("\b", "\\08");
                              /*@.str = constant [5 x i8] c"hola0" ; recuerda el null terminator
                                @message = global bitcast([5 x i8]* @.str to i8*) 
                               */
                              System.out.println("@" + var.getId() + "str = constant [" + (hola.length()+1) + " x i8] c\"" + hola + "\\00\"");
                              System.out.println("@" + var.getId() + " = global i8* bitcast([" + (hola.length()+1) + " x i8]* @" + var.getId() + "str  to i8*)");
                         }
                         /*if (value instanceof String){
                             System.out.println("SHEILAAAAAAAA");
                         }
                         
                         Object value = ((ValueExpr)e).getValue();
                         if(value instanceof String) {
                              String hola =  ((String)value).replace("\n", "\\0A").replace("\f", "\\0C").replace("\t", "\\09").replace("\b", "\\08");
                              /*@.str = constant [5 x i8] c"hola0" ; recuerda el null terminator
                                @message = global bitcast([5 x i8]* @.str to i8*) 
                               */
                            /*  System.out.println("@" + var.getId() + "str = constant [" + (hola.length()+1) + " x i8] c\"" + hola + "\\00\"");
                              System.out.println("@" + var.getId() + " = global i8* bitcast([" + (hola.length()+1) + " x i8]* @" + var.getId() + "str  to i8*)");
                          } */          
                      }
                     //System.out.println("Sheilaaaaaaaaaa" + print(var.getValue(), 0, 0));
                  }
                 else{
                     System.out.println("@" + var.getId() + "str = constant [1 x i8] c\"\\00\"");
                     System.out.println("@" + var.getId() + " = global i8* bitcast([1 x i8]* @" + var.getId() + "str to i8*)");
                 }
               }
            
            else{
                    System.out.println("@" + var.getId() + " = global " + tipo(var.getType()) + " 0");
                            
            }
        }
        else {
            throw new RuntimeException("Unknown feature type " + f.getClass());
        }
    }
    
    /*********************** FIM FEATURE - PRINT ************************************/
    
        /*********************** FEATURE - PRINT2 ************************************/
    private void print2(Feature f, String classtype ) {
        Expr e;
        String resultado;
        if(f instanceof Method) {
            Method m = (Method)f;
            if(m.getName().equals("main")){
                // ATÉ AQUI MAIN NÃO RECEBE NADA
                //System.out.println("call " + tipo(m.getType()) + " @" + m.getName() + "()");
                System.out.println("call %Main* @Main_main(%Main* null)");
            }
                        
        }
        else if (f instanceof Variable) {
            Variable var = (Variable)f;

            
            // AQUI PRECISA IMPLEMENTAR, NÃO SEI COMO VAI VOLTAR O RESULTADO SHEILA
            if( var.getValue() != null && !var.getType().equals("String") ) {
                resultado = print(var.getValue(), 3, 0);
                System.out.println("store " + tipo(var.getType()) + " " + resultado + ", " + tipo(var.getType()) + "* @" + var.getId());
                //store i32 130, i32* @numero
            }
        }
        else {
            throw new RuntimeException("Unknown feature type " + f.getClass());
        }
    }
/***************************** FIM PRINT2 FEATURE ***************************************/
    public static void printIndent(int indent) {
        for (int i = indent; i > 0 ; i-- ) {
            System.out.print("    ");
        }  
    }

    public static void printIndent(int indent, StringBuilder sb) {
        for (int i = indent; i > 0 ; i-- ) {
            sb.append("    ");
        }
    }

    private String printTag(Expr e) {

            String type = e.getExprType();
            return type;

    }

/*********************** EXPR ************************************/
    @SuppressWarnings("unchecked")
    private String print(Expr e, int indent, int autoinc) {
        assert e != null : "node is null";
        String resultado = null;
        
        //printIndent(indent);

        if(e instanceof Block) {
            
            Block block = (Block) e;
            //autoinc = autoinc++;
            
            //System.out.println("SHEILA - bloco");
            for(int b = 0; b < block.getStatements().size(); b ++) {
                if(b != (block.getStatements().size() -1))
                    print(block.getStatements().get(b), indent, autoinc++);
                //retorna o ultimo tipo
                else
                    resultado = print(block.getStatements().get(b), indent, autoinc++);
            }

        }
        else if(e instanceof WhileExpr) {
            WhileExpr loop = (WhileExpr)e;

            //se verdadeiro passa normal
            assert "Object".equals(loop.getExprType()) : "while type must be Object";
            String cond;
            String cond2;

            //printTag("while", e);

            cond = print(loop.getCond(), indent, autoinc++);
            System.out.println("br i1" +  cond + ", label %Verdade" + autoinc + " , label %Falso" + autoinc);
            System.out.println("Verdade" + autoinc + ":");
            //System.out.println("loop");
            print(loop.getValue(), indent, autoinc++);
            cond2 = print(loop.getCond(), indent, autoinc++);
            System.out.println("br i1" +  cond2 + ", label %Verdade" + autoinc + " , label %Falso" + autoinc);
            System.out.println("Falso" + autoinc + ":");

        }
        else if(e instanceof AssignExpr) {
            String valor;
            StringBuilder resultadoAux = new StringBuilder();
            //printTag(String.format("assign %s", ((AssignExpr)e).getId()), e);

            valor = print(((AssignExpr)e).getValue(), indent+1, autoinc++);
            resultadoAux.append("@").append(((AssignExpr)e).getId());
            resultado = resultadoAux.toString();
            System.out.println("store " + tipo(printTag(e)) + " " + valor + ", " + tipo(printTag(e)) +"* @" + ((AssignExpr)e).getId());
        }        
        else if(e instanceof DispatchExpr) {
            DispatchExpr call = (DispatchExpr)e;

            StringBuilder out = new StringBuilder();
            StringBuilder resultadoAux = new StringBuilder();
            String palabra = null;
            // precisa ficar num array list porque vai ter as contas e a resposta vem só depois
            ArrayList<String> argumentos = new ArrayList<String> ();
            int autoincDis = autoinc;
            

            if(call.getName().equals("out_string")){
                palabra = print(call.getArgs().get(0), indent, autoinc++);
                
                //Expr expr = call.getArgs().get(0);
                /*if (expr instanceof ValueExpr){
                    Object value = ((ValueExpr)expr).getValue();
                    if(value instanceof String){
                        String straux = (String)value;
                        straux = straux.replace("\n", "").replace("\f", "").replace("\t", "").replace("\b", "").replace(" ", "").replace("!", "excl" ).replace("\"", "barradir" ).
                        replace("#","num" ).replace("%", "porc").replace("&", "ecom" ).replace("'", "aspinha").replace("(","parendir" ).replace(")","parenis" ).
                        replace("*","aste" ).replace("+", "mais").replace("-","menos" ).replace(",", "vir").replace(":", "doispto").replace(";", "ptovir").
                        replace("<", "menor").replace("=", "igual").replace(">", "maior").replace("?","inte" ).replace("@", "arroba").replace("[","colchdir" ).
                        replace("\\", "barrais" ).replace("]", "colcheis").replace("^","circun" ).replace("`", "agudo").replace("{", "chadir").replace("|", "reto").
                        replace("}", "chaveis").replace("~", "til");
                        
                         out.append("%impr_str").append(autoincDis).append(" = alloca i8*\n")
                                 .append("store i8* bitcast ([").append(((String)value).length()).append(" x i8]* @").append(straux).append(" to i8*), i8** %impr_str").append(autoincDis).append("\n")
                                 .append("%impr2_str").append(autoincDis).append(" = load i8** %impr_str").append(autoincDis).append("\n")
                                 .append("call %IO* @IO_out_string(%IO* null, i8* %impr2_str").append(autoincDis).append(")");
                         System.out.println(out);
                    }
                } */
                System.out.println("call %IO* @IO_out_string(%IO* null, i8* " + palabra + ")");
                    
            }
            
            else if(call.getName().equals("out_int")){
                

                palabra = print(call.getArgs().get(0), indent, autoinc++);
                System.out.println("call %IO* @IO_out_int(%IO* null, i32 " + palabra + ")");
            }
            
            else if(call.getName().equals("in_string")){
                System.out.println("%inString" + autoincDis + " = call i8* @IO_in_string(%IO* null)");
                resultado = "%inString" + autoincDis;
            }
            
            else if(call.getName().equals("in_int")){
                System.out.println("%inInt" + autoincDis + " = call i32 @IO_in_int(%IO* null)");
                resultado = "%inInt" + autoincDis;
                
            }
            else{
                if(call.getExpr() == null){
                    if (call.getArgs().size() > 0) {
                        //printIndent(indent+1);
                        //System.out.println("args");
                        for(Expr arg: call.getArgs()) {
                            argumentos.add(tipo(printTag(arg)) +" "+ print(arg, indent, autoinc++));                    
                        }
                    }

                    //call %IO* @IO_out_int(%IO* %_tmp_1, i32 %val)
                    out.append("call ").append(tipo(printTag(e))).append(" @").append(call.getName()).append(" (");

                    //System.out.printf("call " + tipo(printTag(e)) + " @" + call.getName() + " (");

                   for(int o = 0; o < argumentos.size(); o ++) {

                        out.append(argumentos.get(o));
                        if (o != (argumentos.size() - 1)){;
                            out.append(", ");
                        }
                    }

                    //System.out.printf(")\n");
                   out.append(")\n ");
                   if(!printTag(e).equals("void")){
                       System.out.println("%llamaF" + autoincDis + " = " + out);
                       resultadoAux.append("%llamaF").append(autoincDis);
                       resultado = resultadoAux.toString();
                   }
                   else{
                       System.out.println(out);
                   }
                    //caso todas as impressões derem errado, usar esse
                    /*out.append("call ").append(call.getName());
                    if(call.getType() != null) {
                        out.append(" as ").append(call.getType());
                    }*/
                }
                // Existe para string
                else  {
                    //printIndent(indent+1);
                    //System.out.println("callee");
                    // es este ejemplo, es string

                    //FALTA ISSO SHEILA, OS TIPOS DA STRING
                    //resposta = print(call.getExpr(), indent, autoinc++);
                }
            }

        }
        else if(e instanceof IfExpr) {
            //System.out.println("SHEILA - if");
            IfExpr cond = (IfExpr)e;
            String condif;
            int autoincIf = autoinc;
            String resultado1;
            String resultado2;
            StringBuilder out = new StringBuilder();
            StringBuilder out2 = new StringBuilder();

            //printTag("if", e);
            condif = print(cond.getCond(), indent, autoinc++);

            //assert "Bool".equals(cond.getCond().getExprType());
            //br i1 %cond, label %Verdade, label %Falso
            System.out.println("br i1 " + condif + ", label %Verdadeif" + autoincIf + ", label %Falsoif" + autoincIf);
            System.out.println("Verdadeif" + autoincIf + ":");
            resultado1 = print(cond.getTrue(), indent+1, autoinc++);
            System.out.println("br label %Jumpif" + autoincIf);
            
            System.out.println("Falsoif" + autoincIf + ":");
            resultado2 = print(cond.getFalse(), indent+1, autoinc++);
            System.out.println("br label %Jumpif" + autoincIf);
            System.out.println("Jumpif" + autoincIf + ":");
            
            //%.tmp_if_ret3 = phi %IO* [ %.tmp_out_ret3, %.ifTrue0 ], [ %.tmp_out_ret4, %.ifFalse0 ]
            out.append("%resultIf").append(autoincIf).append(" = phi ").append(tipo(cond.getExprType())).append(" [").
                    append(resultado1).append(", %Verdadeif").append(autoincIf).append(" ], [").append(resultado2).
                    append(", %Falsoif").append(autoincIf).append("]");
            System.out.println(out);
            resultado = "%resultIf" + autoincIf;
            //POR ENQUANTO SEM RESULTADO
            

        }
        else if(e instanceof NewExpr) 
        {
            //NO SE USA PORQUE NO HAY MÁS DE UNA CLASE

        }
        else if(e instanceof UnaryExpr) {
            UnaryExpr expr = (UnaryExpr)e;
            String unario;
            StringBuilder out2 = new StringBuilder();
            StringBuilder resultadoAux = new StringBuilder();
            
            unario = print(expr.getValue(), indent + 1, autoinc++);
            
            if(!operator(expr.getOp()).equals("isvoid")){
                out2.append("%unary").append(autoinc).append(" = xor ").append(printTag(e)).append(" ").append(unario).append(", -1");
                resultadoAux.append("%unary").append(autoinc);
                resultado = resultadoAux.toString();
                System.out.println(out2);
            }
            //printTag(String.format("unary %s", operator(expr.getOp())), e);
            
        }
        else if(e instanceof BinaryExpr) {
            BinaryExpr expr = (BinaryExpr)e;
            String derecha;
            String izquierda;
            String operacao;
            StringBuilder out = new StringBuilder();
            StringBuilder resultadoAux = new StringBuilder();
            int autoinc2 = autoinc + 2;
            
            derecha = print(expr.getLeft(), indent, autoinc++);   
            izquierda = print(expr.getRight(), indent, autoinc2);
            operacao = operator(expr.getOp());
            
            if(operacao.equals("+")){
                //<result> = add i32 4, %var
                out.append("%soma").append(autoinc).append(" = add ").append(tipo(printTag(e))).append(" ").append(izquierda).append(", ").append(derecha);
                resultadoAux.append("%soma").append(autoinc);
                resultado = resultadoAux.toString();
                
            }
            else if(operacao.equals("-")){
                out.append("%subtr").append(autoinc).append(" = sub ").append(tipo(printTag(e))).append(" ").append(izquierda).append(", ").append(derecha);
                resultadoAux.append("%subtr").append(autoinc);
                resultado = resultadoAux.toString();
                
            }
            else if(operacao.equals("*")){
                out.append("%mult").append(autoinc).append(" = mul ").append(tipo(printTag(e))).append(" ").append(izquierda).append(", ").append(derecha);
                resultadoAux.append("%mult").append(autoinc);
                resultado = resultadoAux.toString();
                
            }
            else if(operacao.equals("/")){
                out.append("%div").append(autoinc).append(" = sdiv ").append(tipo(printTag(e))).append(" ").append(izquierda).append(", ").append(derecha);
                resultadoAux.append("%div").append(autoinc);
                resultado = resultadoAux.toString();
                
            }
            else if(operacao.equals("<")){
                out.append("%menor").append(autoinc).append(" = icmp slt ").append(tipo(printTag(e))).append(" ").append(izquierda).append(", ").append(derecha);
                resultadoAux.append("%menor").append(autoinc);
                resultado = resultadoAux.toString();
            }
            else if(operacao.equals("<=")){
                out.append("%menorI").append(autoinc).append(" = icmp sle ").append(tipo(printTag(e))).append(" ").append(izquierda).append(", ").append(derecha);
                resultadoAux.append("%menorI").append(autoinc);
                resultado = resultadoAux.toString();
                
            }
            else if(operacao.equals("=")){
                out.append("%igual").append(autoinc).append(" = icmp eq ").append(tipo(printTag(e))).append(" ").append(izquierda).append(", ").append(derecha);
                resultadoAux.append("%igual").append(autoinc);
                resultado = resultadoAux.toString();
                
            }
            

        }
        else if (e instanceof CaseExpr) {
            
            //SEGUN EL ASSIGNMENT 4, NO VAMOS A USAR CASE


        }
        else if (e instanceof LetExpr) {
            //SEGUN EL ASSIGNMENT 4, NO VAMOS A USAR LET

        }
        else if (e instanceof IdExpr) {
            StringBuilder out = new StringBuilder();
            StringBuilder resultadoAux = new StringBuilder();
            IdExpr f = ((IdExpr)e);
            //%val = load i32* %ptr
            // AQUI PRECISA CONSULTAR A TABELA DE SIMBOLOS SHEILA - PAREI AQUI
        
            out.append("%l").append(((IdExpr)e).getId()).append(autoinc).append(" = load ").append(tipo(printTag(e))).append("* ").append("@").append(((IdExpr)e).getId());
            resultadoAux = resultadoAux.append("%l").append(((IdExpr)e).getId()).append(autoinc);
            resultado = resultadoAux.toString();
            System.out.println(out);
            
                
        }
        else if(e instanceof ValueExpr) {
            Object value = ((ValueExpr)e).getValue();

            if(value instanceof String) {
                String straux = (String) value;
                StringBuilder out = new StringBuilder();
                straux =  straux.replace("\n", "").replace("\f", "").replace("\t", "").replace("\b", "").replace(" ", "").replace("!", "excl" ).replace("\"", "barradir" ).
                        replace("#","num" ).replace("%", "porc").replace("&", "ecom" ).replace("'", "aspinha").replace("(","parendir" ).replace(")","parenis" ).
                        replace("*","aste" ).replace("+", "mais").replace("-","menos" ).replace(",", "vir").replace(":", "doispto").replace(";", "ptovir").
                        replace("<", "menor").replace("=", "igual").replace(">", "maior").replace("?","inte" ).replace("@", "arroba").replace("[","colchdir" ).
                        replace("\\", "barrais" ).replace("]", "colcheis").replace("^","circun" ).replace("`", "agudo").replace("{", "chadir").replace("|", "reto").
                        replace("}", "chaveis").replace("~", "til");
                out.append("%l").append(straux).append(" = load i8** @").append(straux);
                System.out.println(out);
                resultado = "%l" + straux;
                /* Acredito que não seja necessário porque sempre vai chamar o resultado */
                //System.out.printf(resultado);
            }
            else if(value instanceof Integer) {
                
                assert "Int".equals(e.getExprType());
                resultado = value.toString();
                //System.out.printf(resultado);
                //printTag(String.format("int %d", value), e);

            }
            else if(value instanceof Boolean) {
                //System.out.println("SHEILA - Boolean");
                int hola = 0; 
                assert "Bool".equals(e.getExprType());
                //printTag(String.format("bool %s", value), e);
                
                /*	;%ptr = alloca i32
                        ;store i32 3, i32* %ptr
                        ;%val = load i32* %ptr */   
                if((Boolean) value){
                    hola = 1;
                    resultado = "" + hola;
                }
                
                resultado = "" +  hola;
                //System.out.printf(resultado);
            }
            else {
                throw new RuntimeException(String.format("Unsupported constant type %s\n", e.getClass()));
            }
        }
        else {

            if( e != null) {
                throw new RuntimeException(String.format("Unsupported node type %s\n", e.getClass()));
            }
            else {
                throw new RuntimeException(String.format("Null node", e.getClass()));
            }

        }
        return resultado;
    }


    public String operator(UnaryOp op) {
        switch(op) {
            case ISVOID:    return "isvoid";
            case NEGATE:    return "~"; 
            case NOT:       return "not";
        }

        return null;
    }


    public String operator(BinaryOp op) {
        switch(op) {
            case PLUS:      return "+";
            case MINUS:     return "-";
            case MULT:      return "*";
            case DIV:       return "/";            
            case LT:        return "<";
            case LTE:       return "<=";
            case EQUALS:    return "=";
        }
        return null;
    }
    
    public String tipo(String tipoEscrito){
        if (tipoEscrito.equals("String"))
            return "i8*";
        else if(tipoEscrito.equals("Bool"))
            return "i1";
        else if(tipoEscrito.equals("Int"))
            return "i32";
        else if(tipoEscrito.equals("IO"))
            return "%IO*";
        // todos heredan de object
        else
            return "%Object*";
    }

    
}